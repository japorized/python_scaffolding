set dotenv-load

alias help := default
alias h := default

virtenv_dir := ".venv"
stub_dir    := ".stubs"

# Show this help
default:
  @just --list --unsorted

# Scaffold your new python project
setup:
  #!/usr/bin/env bash
  if [[ -d "{{virtenv_dir}}" ]]; then
    echo "{{virtenv_dir}} already exists. Exiting setup procedure."
    exit 1
  fi

  python -m venv "{{virtenv_dir}}"
  echo "Virtual environment ready."

  if [[ ! -d ".git" ]]; then
    echo -n "The current dir is not tracked with git. git init? (y/N)"
    read -srn 1 should_init_git ; echo
    if [[ "${should_init_git@L}" == "y" ]]; then
      git init
      just _setup_gitignore
    fi
  fi

  just _setup_env

# Do something within the virtual env
virtenv +ARGS:
  #!/usr/bin/env bash
  if [[ ! -d "{{virtenv_dir}}" ]]; then
    echo -n "Virtual environment isn't setup. Set it up now? (y/N)"
    read -srn 1 should_setup_virtenv ; echo
    if [[ "${should_setup_virtenv@L}" == "y" ]]; then
      just setup
    else
      echo "Project has no virtual env. Halting..."
      exit 1
    fi
  fi
  source "{{virtenv_dir}}/bin/activate"
  {{ARGS}}

# Run python cli within virtual env
python +ARGS="":
  @just virtenv python {{ARGS}}

# Do something with pip in the virtual env
pip +ARGS="":
  @just python -m pip {{ARGS}}

# Print requirements.txt from the virtual env
freeze_deps:
  @just pip freeze > requirements.txt

# Install a python pkg in the virt env with pip
install +PACKAGES:
  @just pip install {{PACKAGES}}

# Use python's unittest cli
unittest +ARGS="":
  @just python -m unittest {{ARGS}}

# Test all tests
test_all:
  @just unittest discover -s "$SRC_DIR" -p "*_test.py"

# Create a new test file at PATH
create_test PATH:
  cp "{{stub_dir}}/unittest.py" "$SRC_DIR/{{PATH}}"

_setup_gitignore:
  #!/usr/bin/env bash
  gitignore_list=("*.env" ".mypy_cache/" "**/__pycache__/" "{{virtenv_dir}}/")
  if [[ ! -f ".gitignore" ]]; then
    echo -n "Setup .gitignore? (y/N)"
    read -srn 1 should_setup_gitignore ; echo
    if [[ "${should_setup_gitignore@L}" == "y" ]]; then
      printf '%s\n' "${gitignore_list[@]}" > .gitignore
    fi
  else
    echo "Add these to .gitignore?"
    printf '%s\n' "${gitignore_list[@]}"
    read -p "(y/N)" -srn 1 should_setup_gitignore ; echo
    if [[ "${should_setup_gitignore@L}" == "y" ]]; then
      printf '%s\n' "${gitignore_list[@]}" >> .gitignore
    fi
  fi

_setup_env:
  #!/usr/bin/env bash
  echo "What's the name of your starting module?"
  read -r src_dir
  retval=$?
  if [[ $retval -eq 0  ]] && [[ -n "$src_dir" ]]; then
    mkdir -p "$src_dir"
    echo "SRC_DIR=$src_dir" >> .env
  fi
