# python_scaffolding

A simple scaffolding for my Python projects.

---

## How to use

### Requirements

- [just](https://github.com/casey/just)
- python 3
  - `unittest`
- bash

### Setup

1. `git clone https://gitlab.com/japorized/python_scaffolding.git <YOUR_NEW_PROJECT>`
2. `cd <YOUR_NEW_PROJECT>`
3. `rm -drf .git README.md`
4. `just setup`
5. ?
6. Profit

### In Dev

```
just help
```
